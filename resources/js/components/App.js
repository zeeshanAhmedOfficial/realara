import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            BASE_URL : "http://localhost/proj-mongo-ang-with-react/public/",
            users : [],
        };
    } 

    componentDidMount() {
        this.getUsers();
    }

    getUsers() {
        axios.get(this.state.BASE_URL+'api/users').then((response) => {
            this.setState({
                users : response.data
            });
        }).catch((error) => {
            console.log('Some thing went wrong ' + error);
        });
    }

    list() {
        return (
            <tr>
            </tr>
        );
    }

    render() {
                            console.log(this.state.users.map(({user}) => {
                                user
                            }));
        return (
            <div className="row justify-content-center">
                <div className="col-md-12">
                    Listing Users
                  
                    <table className="table">
                        <thead className="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Verification Status</th>
                                <th scope="col">Added On</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('userListReact'));
