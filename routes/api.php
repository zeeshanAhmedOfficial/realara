<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/users', function() {
	return \App\Models\User::all();
});

Route::prefix('projects')->group(function() {
    $this->get('/', 'Api\ProjectsController@index');
    $this->post('/', 'Api\ProjectsController@store');
    $this->get('/{id}', 'Api\ProjectsController@show');
    $this->put('/{project}', 'Api\ProjectsController@markAsCompleted');
});

Route::prefix('tasks')->group(function() {
    $this->post('/', 'Api\TasksController@store');
    $this->put('/{task}', 'Api\TasksController@markAsCompleted');
});
