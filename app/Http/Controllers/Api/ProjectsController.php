<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Project;

class ProjectsController extends Controller
{
    public function index() {
        $projects = Project::where('is_completed', false)
                            ->orderBy('created_at', 'DESC')
                            ->withCount(['tasks' => function($query) {
                                return $query->where('is_completed', false);
                            }])
                            ->get();

        return $projects;
    }

    public function store(Request $request) {
        $validateData = $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $project = Project::create([
            'name' => $request->input('name'),
            'description' => $validateData['description'],
        ]);

        return response()->json('Project created!');
    }

    public function show(Project $project) {
        $project->with(['tasks' => function($query) {
            return $query->where('is_completed', false);
        }]);

        return $project;
    }

    public function markAsCompleted(Project $project) {
        $project->is_completed = true;
        $project->update();
        return response()->json('Project updated!');
    }
}
